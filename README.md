# Emilia GRUB

[![pipeline status](https://gitlab.com/emilia-system/emilia-grub/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-grub/commits/master)
[![License](https://img.shields.io/badge/license-GPL3-red)](https://choosealicense.com/licenses/gpl-3.0/)

This package is temporary, seen it's a "fork" from the Vimix theme. This package is identical to the Vimix theme, but added the Emilia icon, and changed the background.

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[GPL3](https://choosealicense.com/licenses/gpl-3.0/)

# Emilia GRUB pt_BR

Este pacote é temporário, visto que é um "fork" do tema Vimix. Este pacote é idêntico ao tema Vimix, porém adiciona o ícone da Emília, e troca o papel de parede.

## Instalação

Copie o tema Vimix instalado, e coloque o tema da Emilia e o papel de parede no lugar.

## Contribuindo
 - Este tema é temporário, visto que é usado mais para testes. Caso queira fazer um tema contribuir, nos contate!

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Problemas conhecidos

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[GPL3](https://choosealicense.com/licenses/gpl-3.0/)
